# Unleashed Technologies: Front End Developer Test

## Summary
While following the [project prompt](#prompt) and striving to incorporate as many [bonus functionality items](#bonus_functionality) as I could, I began with writing the underlying HTML structure of the phonebook application and designing the data model as a JavaScript object. Particular attention was taken to style the application in-line with the Unleashed Technologies branding. As well, the DOM was created with accessibility features in mind and the application can be easily used with only a keyboard or with assistive technologies like a screen reader.

My approach was to use a few external dependencies as possible while still creating a functional, visually pleasing app that has a manageable codebase. For me, "manageable codebase" means that the application is cleanly written and relies very little on external dependencies. This app has [very few dependencies](#dependencies) and a conscious effort was made to showcase my creation skills over leveraging pre-built code. There are limitations and points for improvement in my approach, which I will discuss in the [conclusion and improvements](#conclusion) section of this document.

Given this project is a "test" and is supposed to demonstrate my abilities and thought processes, I kept comments in the code files intact to reveal how I worked through things and so my ideas and code fragments are apparent. In a "production" environment I would refine my code and clean it up more than what is presented here, but I think the more unpolished state of my code is revealing of my process and so have left things pretty much as they were while writing the application from scratch.

The app can be downloaded and opened in a web browser. It can also be viewed in the private CodePen I used to rapidly develop the application [here](https://codepen.io/loganb1/pen/756e08be225af63b8d6dd22b982e709b).
## Dependencies
<a id="dependencies"></a>
### Layout and Style
- Bootstrap 4 grid is used for layout purposes. The app is using a variation that only includes the layout grid and not all the utility classes, keeping things focused and lightweight (~25Kb)

- Simple CSS icons were used as opposed to a heavy icon font package. In this case I have used pure CSS icons from the [CSS GG project](https://github.com/astrit/css.gg)

- Google Fonts are included for a bit more polish than system fonts only

### Functionality
- Only the jQuery standard library is included and is used for DOM manipulation. The app could be refactored to be completely vanilla JS but I like the syntactical simplicity of jQuery and I can work very quickly using it. Additionally, if the app were to use an API for external data transfer, the $.ajax() method is available via jQuery to easily make asynchronous HTTP requests.

## Prompt
<a id="prompt"></a>
Create a single page "phone book" application. This application should allow a user add and
remove contacts (consisting of a name phone number). This application will help demonstrate
your front end development abilities.

## Requirements
:heavy_check_mark: The application should be a single web page. Separate asset files (JS, CSS, images,
etc.) are fine.

:heavy_check_mark: The page should display, at minimum, a form for adding new entries and a list

:heavy_check_mark: The user should be able to add an entry, consisting of a name and phone number, and
remove any entry from the list.

:heavy_check_mark: The application should be responsive and aesthetically pleasing. This is where you can
express your creativity

:heavy_check_mark: Third party JavaScript MVC frameworks, JavaScript libraries, CSS frameworks, CSS
processors are permitted and encouraged.

## Bonus Functionality
<a id="bonus_functionality"></a>
Implement at least two of features in this section. You may add any feature or aesthetic changes you
wish as long as it does not hinder the user experience and your app still meets the requirements above.
### Level 0
- ~~Add ability to "Favorite", color code, or categorize entries.~~
- ~~Validate the Phone Number.~~
- ~~Add some color and theme beyond the basic CSS provided by a framework.~~
### Level 1
- ~~Add ability to "Favorite" entries, bringing them to the top of the list.~~
- ~~Ensure that no duplicates are added.~~ NOTE: The interface will remove duplicate listed items and each item has a unique ID but it is possible to submit and store duplicate name/phone records.
- Use a modern JavaScript MVC framework.
### Level 2
- ~~Display the list in alphabetical order.~~
NOTE: I found when testing the app locally that my alpha sorting doesn't work. I used CodePen as a rapid development tool and the button works in my pen. Not enough time to troubleshoot and fix.
- Add ability to edit any entries.

## Conclusion and Improvements
<a id="conclusion"></a>
I really enjoyed working on this small project. There was enough direction for the project to focus on outcomes while not being forced into a specific implementation modality. Because I approached the project thinking in a "MVC" way, I found that it was simple to expand my main "phonebook" class as I added functionality or features. If given more time, I would have liked to translate this build to the ReactJS framework since it directly relates to WordPress and establishes a standard that other developers can jump right into quickly. Given my lack of intimate React familiarity, I went in the direction of getting things done quickly and effectively first; I wanted a great-looking and functioning app even if it meant not using the latest, greatest shiny JavaScript tool(s). But, to my surprise, when I looked around the internet at some example phone book React implementations they very much resembled the way I structured my own application in terms of JavaScript objects and methods. Standard frameworks have their place and can help in multi-contributor development teams for standardization, but many times a framework like React is unnecessary for the project goals and ends up adding bloat and complexity to what would otherwise be a lean and clean project.

Another place this project could be improved is using a CSS preprocessor like LESS or SASS. As projects grow in complexity and contributors, CSS preprocessors make changes easier and provides better style control. Like most things I build, my approach here was minimum viable product (MVP) first and refinement later. Thus, I did not have time to put in fancy development features like a build process, automated tests, or frameworks/libraries. As a primarily front-end developer I do not use a lot of development tooling and prefer the most direct, efficient, and minimal approach that can meet project expectations and goals.

Far too much time was spent on adjusting the interface styling and working on the "favorite" feature that I barely got the alphabetical sorting to work and did not have time to style its button or consider what the interface would look like when adding more user functions like inline record editing, color-coding entries, etc. There was also planned functionality that I did not get to, like exporting the phone book as a CSV file for use in other systems. I like the pressure that a deadline brings and I think I prioritized in a sensible way to result in a project that works as expected and that I am pleased with the final result.
