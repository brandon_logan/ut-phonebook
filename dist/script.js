var $ = jQuery;
var phonebook = {
  lastEdited : "timestamp",
  updateTime : function(){
    let now = new Date();
    this.lastEdited = new Date(now.getTime());
  },
  createEntry : function(name,number,photoURL){
    //let nameArr = name.split(' ');
    let phonebookEntry = {
      //possibility of identical IDs if function is run sequentially within milliseconds
      //consider timeout or other ID generation method for bulk creation process
      entryID : Date.now(),
      // firstName : nameArr[0],
      // lastName : nameArr[1],
      name: name,
      phoneNumber : number,
      isFavorite: false,
      photoURL : photoURL
    };
    this.entries.push(phonebookEntry);
    this.updateTime();
    //save the new entries listing
    //combine all entries before saving
    let allEntries = [].concat(this.favorites,this.entries);
    this.saveEntries(allEntries);
  },
  removeEntry : function(entryID){

    //remove the entry from memory
    let index = this.entries.map(function(e) { return e.entryID; }).indexOf(parseInt(entryID));
    this.entries.splice(index,1);

    this.updateTime();
    //remove the item from the DOM
    $('body').find('#entry-'+entryID).remove();
    //save the new entries listing
    //combine all entries before saving
    let allEntries = [].concat(this.favorites,this.entries);
    this.saveEntries(allEntries);
  },
  getEntries : function(){
    let storedVals = localStorage.getItem('ut_phonebook');
    if (storedVals !== null && storedVals !== ''){
      let storageObj = JSON.parse(storedVals);
      this.entries = storageObj.entries;
      this.filterFaves(this.entries);
      console.log('Retrieved items saved on '+storageObj.saved);
    }else{
      console.log('No phonebook entries are stored.')
    }
  },
  saveEntries : function(entriesArr){
    let now = new Date();
    let storageObj = {entries:entriesArr,saved:new Date(now.getTime())};
    localStorage.setItem('ut_phonebook', JSON.stringify(storageObj));
    console.log('Phonebook entries saved to browser storage');
  },
  entries : [],
  favorites : [],
  filterFaves : function(listArr){
    //takes out favorites to display above normal entries
    const listLength = listArr.length;

    for(i=0;i<listLength;i++){
      let thisItem = listArr[i];
      if (thisItem){
        if (thisItem.isFavorite === true){
          this.favorites.push(thisItem);
        }
      }
    }
    this.renderList();
  },
  exportList : function(){
    //save as a phonebook file
  },
  renderList : function(targetContainer){
    //passes in taget container ID
    //render the list in a container
    //no passed container means listing exists
    if (targetContainer === null || targetContainer == '' || typeof targetContainer === 'undefined'){
      $('body').find('#phonebook-entries').html('');
    }else{
      $('#'+targetContainer).append('<ul id="phonebook-entries"></ul>');
    }
    //output favorited items first
    let totFaves = this.favorites.length;
    for(i=0;i<totFaves;i++){
      let faveEntry = this.favorites[i];
      this.renderEntry(faveEntry);
    }
    let totEntries = this.entries.length;
    for(i=0;i<totEntries;i++){
      let newEntry = this.entries[i];
      if (newEntry.isFavorite === false){
        this.renderEntry(newEntry);
      }
    }
  },
  renderEntry : function(entryObj){
    let thisID = entryObj.entryID;
    let name = entryObj.name;
    let number = entryObj.phoneNumber;
    //format phone number for display, as "(000) 000 0000"
    let formattedNumber = number.substr(0,3)+') '+number.substr(3,3)+' '+number.substr(6,4);
    let isFave = entryObj.isFavorite;
    let faveBtn = this.renderFaveBtn(isFave);

    let template = '<li id="entry-'+thisID+'" class="row entry" data-isFave="'+isFave+'"><div class="col-md-1 col-6 favorite-entry">'+faveBtn+'</div><div class="col-md-4 entry-name"><h3>'+name+'</h3><div class="image-wrapper"><i aria-hidden="true" class="gg-user"></i></div></div><div class="col-md-5 entry-phone"><a href="tel:'+number+'"><i aria-hidden="true" class="gg-phone"></i>('+formattedNumber+'</a></div><div class="col-md-2 col-6 delete-button"><button class="delete-entry"><i aria-hidden="true" class="gg-trash"></i><span class="visually-hidden">Remove Entry</span></button></div>';
    //insert the prepared element
    $('body').find('#phonebook-entries').append(template);
    this.removeDupes();
  },
  setFavorite : function(entryID){
    console.log('set fave for '+entryID);
    let index = this.entries.map(function(e) { console.log(e.entryID);return e.entryID; }).indexOf(parseInt(entryID));
    console.log('match at '+index);
    let faveState = this.entries[index].isFavorite;
    let entryItem = $('#entry-'+entryID);
    //remove the current button
     entryItem.find('.favorite').remove();
    //set or unset fave state based on current state
    //insert new button
    if (faveState === true){
      this.entries[index].isFavorite = false;
      entryItem.find('.favorite-entry').append(this.renderFaveBtn(false));
    }else{
      this.entries[index].isFavorite = true;
      entryItem.find('.favorite-entry').append(this.renderFaveBtn(true));
    }
    this.favorites = [];
    this.filterFaves(this.entries);
    this.saveEntries(this.entries);
  },
  renderFaveBtn : function(isFave){
    //accepts boolean of fave state, returns correct star btn as markup string
    if (isFave === false){
      return '<button class="favorite" title="Mark this entry as a favorite" aria-label="Mark this entry as a favorite"><span class="star">&#9734;</span><span class="visually-hidden">Add to Favorites</span></button>';
    }else{
      return '<button class="favorite is-favorite" title="Remove this entry from favorites" aria-label="Remove this entry from favorites"><span class="star">&#9733;</span><span class="visually-hidden">Remove from Favorites</span></button>';
    }
  },
  validateField : function(name,val){
    if (val !== '' && val !== null){
      switch (name) {
      case 'entry-name' :
        return true;
        break;
      case 'entry-phone' :
        //strip non-numerical chars
        let numbers = val.replace(/\D/g,'');
        //ensure 10 digit phone
        if (numbers.length === 10){
          return true
        }else{
          alert('The phone number should have 10 digits, e.g. 000-000-0000');
          return false;
        }
        break;
      default :
        console.log('Validation field name not found.');
        return false;
        break;
      }
    }else{
      console.log('There is no value for the field.');
      return false;
    }
  },
  entrySubmit : function(){
    let hasError = false;
    let $fields = $('#entry-creation input');
    let name = '';
    let phone = '';
    //validate each phonebook entry field before creating record
    $fields.each(function(){
       let $this = $(this);
       let theID = $this.attr('id');
       let fieldname = theID.split('-')[1];
       let val = $this.val();

       if (fieldname === 'name'){
         name = val;
       }else if(fieldname === 'phone'){
         phone = val;
       }

       if(phonebook.validateField(theID, val) === false){
         $this.addClass('has-error');
         hasError = true;
       }else{
         $this.removeClass('has-error');
       }
    });
    if (hasError === true){
      console.log('Entry failed. Check for errors and try again');
      return false;
    }else{
      this.createEntry(name,phone,null);
      //clear field values after record is added
      $fields.val('');
      //move focus to first field to set up for adding next entry
      $fields.first().focus();
      //re-render the list
      this.renderList();
    }
  },
  removeDupes : function(){
    //removes duplicates from the listing
    let checked = [];
    $('#phonebook-entries li').each(function(){
      let thisID = $(this).attr('id');
      if (checked.indexOf(thisID) > -1){
        $(this).remove();
      }else{
        checked.push(thisID);
      }
    });
  }
}

//Main functions and handlers for after DOM is loaded
$(window).on('load', function(){
  //init
  phonebook.getEntries();

  //bind UI events to phonebook actions
  $('#add-entry').click(function(){ phonebook.entrySubmit() });
  //submit entry creation data on Enter key
  $('#entry-creation input').on('keyup',function(e){
    if (e.key === 'Enter' || e.keyCode === 13) {
      phonebook.entrySubmit();
    }
  });
  //handle clicks on individual entries
  $('body').on('click', '#phonebook-entries li', function(event){
    let thisClass = $(event.target).attr('class');
    //set this entry ID for use later
    let entryID;
    if ($(this).hasClass('entry')){
      entryID = $(this).attr('id').split('-')[1];
    }else{
      entryID = $(this).parents('li').attr('id').split('-')[1];
    }
    //only allow the default behavior for tel link action
    if (!$(event.target).parents('.entry-phone')){
      event.preventDefault();
    }
    //map UI buttons to phonebook actions
    if (thisClass === 'delete-entry' || thisClass == 'gg-trash'){
      console.log('delete button');
      phonebook.removeEntry(entryID);
    }else if(thisClass === 'favorite' || thisClass === 'star'){
      console.log('favorite button');
      phonebook.setFavorite(entryID)
    }
  });
  //Sort entries by entry name in alpha order
  //Should be moved into the phonebook object
  $('#alpha-sort').click(function(event){
    event.preventDefault();
    console.log('sort btn');
      let $listing = $('body').find('#phonebook-entries');
      let $entryItems = $('li[data-isFave="false"]', $listing);
      let $faves = $('li[data-isFave="true"]');

      $faves.sort(function(a, b){
        let keyA = $('h3',a).text();
        let keyB = $('h3',b).text();
        return (keyA > keyB) ? 1 : 0;
      });
      $.each($faves, function(index, item){
        $listing.append(item);
      });
      $entryItems.sort(function(a, b){
        let keyA = $('h3',a).text();
        let keyB = $('h3',b).text();
        return (keyA > keyB) ? 1 : 0;
      });
      $.each($entryItems, function(index, item){
        $listing.append(item);
      });
   })
})
